/* eslint-disable global-require */

const Webpack = require('webpack');
const Clean = require('clean-webpack-plugin');

const isProduction = process.env.NODE_ENV === 'production';

module.exports = {
   entry: { main: './src/entry.js' },

   output: {
      path: './build',
      filename: '[name].js',
   },

   resolve: {
      modulesDirectories: ['node_modules', 'src'],
   },

   debug: !isProduction,
   devtool: isProduction ? false : 'eval',

   module: {
      preLoaders: [],

      loaders: [
         {
            test: /\.js$/i,
            loader: 'babel',
         },
         {
            test: /\.html$/i,
            loader: 'file?name=[name].[ext]!extract!html!posthtml',
         },
         {
            test: /\.css$/i,
            loader: 'file?name=[name].[ext]!extract!css!postcss',
         },
         {
            test: /\.(png|gif|jpe?g|svg)$/i,
            loader: 'url?limit=10240', // 10kb
         },
         {
            test: /\.(woff|woff2)$/i,
            loader: 'file',
         },
      ],
   },

   plugins: [
      new Webpack.DefinePlugin({
         __PRODUCTION__: isProduction,
         __DEVELOPMENT__: !isProduction,
         'process.env': {
            NODE_ENV: JSON.stringify(process.env.NODE_ENV),
            BABEL_ENV: JSON.stringify(process.env.NODE_ENV),
         },
      }),
      new Webpack.optimize.CommonsChunkPlugin({
         name: 'main',
         children: true,
         minChunks: 2,
      }),
      ...(isProduction ? [
         new Clean('build'),
         new Webpack.optimize.DedupePlugin(),
         new Webpack.optimize.OccurenceOrderPlugin(),
         new Webpack.optimize.MinChunkSizePlugin({
            minChunkSize: 51200, // 50kb
         }),
         new Webpack.optimize.UglifyJsPlugin({
            mangle: true,
            compress: { warnings: false },
         }),
      ] : []),
   ],

   posthtml(webpack) {
      const include = require('posthtml-include');

      return [
         include({
            root: './src',
            addDependencyTo: webpack,
         }),
      ];
   },

   postcss(webpack) {
      const imports = require('postcss-import');
      const url = require('postcss-url');
      const cssnext = require('postcss-cssnext');

      return [
         imports({
            addDependencyTo: webpack,
         }),
         url({
            url: 'rebase',
         }),
         cssnext({
            browsers: ['> 1%', 'ie > 8'],
         }),
      ];
   },
};
